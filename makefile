# makefile for my1tcpserve

EXETOOL = my1tcpserve
OBJTOOL = my1sockbase.o my1socksrv.o my1bytes.o my1crc16.o my1keys.o
OBJTOOL += my1tcpdat.o my1tcpsrv.o my1tcpserve.o

EXENEXT = my1tcpcheck
OBJNEXT = my1sockbase.o my1sockcli.o
OBJNEXT += my1bytes.o my1crc16.o my1tcpdat.o my1tcpcheck.o

EXTPATH = ../my1codelib/src

CFLAG += -Wall -I$(EXTPATH)
CFLAG += -DMY1APP_PROGVERS=\"$(shell date +%Y%m%d)\"
CFLAG +=
LFLAG +=

RM = rm -f
CC = gcc -c
LD = gcc

.PHONY: all new debug serve check clean

debug: CFLAG += -g -DMY1_DEBUG

all: $(EXETOOL) $(EXENEXT)

new: clean all

debug: new

serve: $(EXETOOL)

check: $(EXENEXT)

${EXETOOL}: $(OBJTOOL)
	$(LD) $(CFLAG) -o $@ $+ $(LFLAG)

${EXENEXT}: $(OBJNEXT)
	$(LD) $(CFLAG) -o $@ $+ $(LFLAG)

%.o: src/%.c src/%.h
	$(CC) $(CFLAG) -o $@ $<

%.o: src/%.c
	$(CC) $(CFLAG) -o $@ $<

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) $(CFLAG) -o $@ $<

clean:
	-$(RM) $(EXETOOL) $(EXENEXT) *.o
