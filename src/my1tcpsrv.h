/*----------------------------------------------------------------------------*/
#ifndef __MY1TCPSRV_H__
#define __MY1TCPSRV_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1 TCP server library
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
#include "my1socksrv.h"
#include "my1tcpdat.h"
/*----------------------------------------------------------------------------*/
#define TCPSRV_PORT 1337
#define TCPSRV_BUFFSIZE 4096
/*----------------------------------------------------------------------------*/
typedef int (*event_t)(void*,void*,void*); /* object, conn, temp */
/*----------------------------------------------------------------------------*/
typedef struct _my1tcpsrv_t {
	my1bytes_t ibuf, obuf;
	my1bytes_t idat, odat;
	my1socksrv_t server;
	my1tcpdat_t tcpd;
	void *data; /* general purpose data */
	event_t on_exec, on_done, on_none; /* on_none when socket is unavailable */
	event_t on_connect, on_abort, on_request, on_error;
	/* events when no data received and errno is NOT (EAGAIN OR EWOULDBLOCK) */
	event_t on_zero_data, on_chk_errno;
} my1tcpsrv_t;
/*----------------------------------------------------------------------------*/
void tcpsrv_init(my1tcpsrv_t* tcpsrv,int port);
void tcpsrv_free(my1tcpsrv_t* tcpsrv);
void tcpsrv_exec(my1tcpsrv_t* tcpsrv);
/*----------------------------------------------------------------------------*/
#endif /** __MY1TCPSRV_H__ */
/*----------------------------------------------------------------------------*/
