/*----------------------------------------------------------------------------*/
#ifndef __MY1TCPDAT_H__
#define __MY1TCPDAT_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1 TCP data structure
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
#include "my1bytes.h"
#include "my1crc16.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1tcpdat_t {
	unsigned short size; /* data size in bytes (include uuid&type&checksum) */
	unsigned short uuid; /* node id */
	unsigned short type; /* node type */
	unsigned short csum; /* crc-16 checksum */
	my1crc16_t dcrc;
	my1bytes_t data;
} my1tcpdat_t;
/*----------------------------------------------------------------------------*/
#define TCPDAT_TYPE_TX 0x1000
#define TCPDAT_TYPE_RX 0x2000
#define TCPDAT_TYPE_OK 0x4000
#define TCPDAT_TYPE_QQ 0x8000
/*----------------------------------------------------------------------------*/
void tcpdat_init(my1tcpdat_t* tcpd);
void tcpdat_free(my1tcpdat_t* tcpd);
int tcpdat_dopack(my1tcpdat_t* tcpd, my1bytes_t* pack);
int tcpdat_unpack(my1tcpdat_t* tcpd, my1bytes_t* pack);
/*----------------------------------------------------------------------------*/
#endif /** __MY1TCPDAT_H__ */
/*----------------------------------------------------------------------------*/
