/*----------------------------------------------------------------------------*/
#include "my1tcpsrv.h"
/*----------------------------------------------------------------------------*/
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
int socksrv_code(void* stuffs) {
	int flag, temp;
	my1conn_t *pcon = (my1conn_t*) stuffs;
	my1socksrv_t *psrv = (my1socksrv_t*) pcon->data;
	my1tcpsrv_t *ptcp = (my1tcpsrv_t*) psrv->data;
	/* event */
	if (ptcp->on_connect)
		ptcp->on_connect(ptcp,pcon,0x0);
	/* clear buffer */
	ptcp->idat.fill = 0;
	ptcp->odat.fill = 0;
	/* loop to get all request data */
	while (1) {
		temp = sock_peek(pcon->sock,ptcp->ibuf.size,(void*)ptcp->ibuf.data);
		if (temp>0) {
			bytes_more(&ptcp->idat,ptcp->ibuf.data,temp);
			printf("@@ Size:%d/%d/%d\n",temp,ptcp->ibuf.size,ptcp->idat.fill);
			if (temp<ptcp->ibuf.size) break;
		}
		else {
			/* continue only if errno is EAGAIN or EWOULDBLOCK? */
			if (errno!=EAGAIN&&errno!=EWOULDBLOCK) {
				/* event */
				if (ptcp->on_chk_errno)
					ptcp->on_chk_errno(ptcp,pcon,(void*)&errno);
				break;
			}
		}
	}
	/* check input buffer */
	if (!ptcp->idat.fill) {
		/* event */
		if (ptcp->on_zero_data)
			ptcp->on_zero_data(ptcp,pcon,0x0);
		return 0;
	}
	flag = 0; /* set to non-zero to stop server loop */
	/* process idat */
	if (!tcpdat_unpack(&ptcp->tcpd,&ptcp->idat)) {
		/* event: invalid frame */
		if (ptcp->on_error)
			ptcp->on_error(ptcp,pcon,0x0);
		/* ignore invalid frame */
	}
	else {
		/* event: valid frame */
		if (ptcp->on_request)
			ptcp->on_request(ptcp,pcon,(void*)&ptcp->tcpd);
		else {
			/* default reply! */
			ptcp->tcpd.uuid = 0xffff;
			ptcp->tcpd.type = TCPDAT_TYPE_RX|TCPDAT_TYPE_OK;
			ptcp->tcpd.data.fill = 0;
			tcpdat_dopack(&ptcp->tcpd,&ptcp->odat);
			sock_send(pcon->sock,ptcp->odat.fill,(void*)ptcp->odat.data);
		}
	}
	/* done */
	return flag;
}
/*----------------------------------------------------------------------------*/
int socksrv_done(void* stuffs) {
	my1conn_t *pcon = (my1conn_t*) stuffs;
	my1socksrv_t *psrv = (my1socksrv_t*) pcon->data;
	my1tcpsrv_t *ptcp = (my1tcpsrv_t*) psrv->data;
	if (get_keyhit()==KEY_ESCAPE) {
		if (ptcp->on_abort)
			ptcp->on_abort(ptcp,pcon,0x0);
		return -1; /* return non-zero to abort server loop */
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
void tcpsrv_init(my1tcpsrv_t* tcpsrv,int port) {
	bytes_init(&tcpsrv->ibuf);
	bytes_make(&tcpsrv->ibuf,TCPSRV_BUFFSIZE);
	bytes_init(&tcpsrv->obuf);
	bytes_make(&tcpsrv->obuf,TCPSRV_BUFFSIZE);
	bytes_init(&tcpsrv->idat);
	bytes_init(&tcpsrv->odat);
	socksrv_init(&tcpsrv->server,port);
	tcpdat_init(&tcpsrv->tcpd);
	tcpsrv->data = 0x0;
	tcpsrv->on_exec = 0x0;
	tcpsrv->on_done = 0x0;
	tcpsrv->on_none = 0x0;
	tcpsrv->on_connect = 0x0;
	tcpsrv->on_abort = 0x0;
	tcpsrv->on_request = 0x0;
	tcpsrv->on_error = 0x0;
	tcpsrv->on_zero_data = 0x0;
	tcpsrv->on_chk_errno = 0x0;
	/* initialize server */
	tcpsrv->server.code = &socksrv_code;
	tcpsrv->server.done = &socksrv_done;
	tcpsrv->server.data = (void*) tcpsrv;
}
/*----------------------------------------------------------------------------*/
void tcpsrv_free(my1tcpsrv_t* tcpsrv) {
	tcpdat_free(&tcpsrv->tcpd);
	socksrv_free(&tcpsrv->server);
	bytes_free(&tcpsrv->odat);
	bytes_free(&tcpsrv->idat);
	bytes_free(&tcpsrv->obuf);
	bytes_free(&tcpsrv->ibuf);
}
/*----------------------------------------------------------------------------*/
void tcpsrv_exec(my1tcpsrv_t* tcpsrv) {
	if (socksrv_prep(&tcpsrv->server)) {
		if (tcpsrv->on_none)
			tcpsrv->on_none(tcpsrv,0x0,0x0);
		return;
	}
	if (tcpsrv->on_exec)
		tcpsrv->on_exec(tcpsrv,0x0,0x0);
	socksrv_exec(&tcpsrv->server);
	if (tcpsrv->on_done)
		tcpsrv->on_done(tcpsrv,0x0,0x0);
}
/*----------------------------------------------------------------------------*/
