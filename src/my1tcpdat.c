/*----------------------------------------------------------------------------*/
#include "my1tcpdat.h"
/*----------------------------------------------------------------------------*/
void tcpdat_init(my1tcpdat_t* tcpd) {
	tcpd->size = 0;
	tcpd->uuid = 0;
	tcpd->type = 0;
	tcpd->csum = 0;
	bytes_init(&tcpd->data);
	crc16_init(&tcpd->dcrc);
	/* using table implementation */
	crc16_make_ptab(&tcpd->dcrc);
}
/*----------------------------------------------------------------------------*/
void tcpdat_free(my1tcpdat_t* tcpd) {
	crc16_free(&tcpd->dcrc);
	bytes_free(&tcpd->data);
}
/*----------------------------------------------------------------------------*/
int tcpdat_dopack(my1tcpdat_t* tcpd, my1bytes_t* pack) {
	pack->fill = 0;
	tcpd->size = tcpd->data.fill + 6; /* uuid+type+csum */
	bytes_more(pack,(byte08_t*)&tcpd->size,2);
	bytes_more(pack,(byte08_t*)&tcpd->uuid,2);
	bytes_more(pack,(byte08_t*)&tcpd->type,2);
	bytes_more(pack,(byte08_t*)tcpd->data.data,tcpd->data.fill);
	tcpd->dcrc.ccrc = CRC16_CCITT_INIT; /* always use this */
	crc16_calc(&tcpd->dcrc,pack->data,pack->fill);
	tcpd->csum = tcpd->dcrc.ccrc;
	bytes_more(pack,(byte08_t*)&tcpd->csum,2);
	return pack->fill;
}
/*----------------------------------------------------------------------------*/
int tcpdat_unpack(my1tcpdat_t* tcpd, my1bytes_t* pack) {
	unsigned short* ptmp;
	int stop;
	tcpd->data.fill = 0;
	if (pack->fill<8) return 0;
	ptmp = (unsigned short*)pack->data;
	tcpd->size = *ptmp;
	if ((tcpd->size+2)!=pack->fill) return 0;
	stop = pack->fill-2;
	tcpd->dcrc.ccrc = CRC16_CCITT_INIT; /* always use this */
	crc16_calc(&tcpd->dcrc,pack->data,stop);
	ptmp = (unsigned short*)&pack->data[stop];
	tcpd->csum = *ptmp;
	if (tcpd->dcrc.ccrc!=tcpd->csum) return 0;
	ptmp = (unsigned short*)&pack->data[2];
	tcpd->uuid = *ptmp;
	ptmp = (unsigned short*)&pack->data[4];
	tcpd->type = *ptmp;
	/* copy data */
	bytes_more(&tcpd->data,&pack->data[6],tcpd->size-6);
	return pack->fill;
}
/*----------------------------------------------------------------------------*/
