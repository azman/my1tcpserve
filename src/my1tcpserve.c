/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "my1tcpsrv.h"
/*----------------------------------------------------------------------------*/
#define ERROR_ARGS_OPT (FLAG_ERROR|0x01)
#define ERROR_ARGS_PAR (FLAG_ERROR|0x02)
#define ERROR_ARGS_PORT (FLAG_ERROR|0x04)
/*----------------------------------------------------------------------------*/
typedef struct _data_t {
	FILE* plog;
	int flag, port;
} data_t;
/*----------------------------------------------------------------------------*/
int tcp_connect(void* that,void* conn,void* temp) {
	(void)temp;
	my1tcpsrv_t* ptcp = (my1tcpsrv_t*) that;
	my1conn_t* pcon = (my1conn_t*) conn;
	data_t* data = (data_t*) ptcp->data;
	if (data->plog)
		fprintf(data->plog,"Request from {%s}!\n",pcon->addr);
	return 0;
}
/*----------------------------------------------------------------------------*/
int tcp_abort(void* that,void* conn,void* temp) {
	(void)conn; (void)temp;
	my1tcpsrv_t* ptcp = (my1tcpsrv_t*) that;
	data_t* data = (data_t*) ptcp->data;
	if (data->plog)
		fprintf(data->plog,"User Abort Requested!\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
int tcp_protocol_error(void* that,void* conn,void* temp) {
	(void)temp;
	my1tcpsrv_t* ptcp = (my1tcpsrv_t*) that;
	my1conn_t* pcon = (my1conn_t*) conn;
	data_t* data = (data_t*) ptcp->data;
	if (data->plog) {
		int loop;
		fprintf(data->plog,"Invalid TCP data protocol from client:{%s}!\n",
			pcon->addr);
		fprintf(data->plog,"Packet:\n");
		for (loop=0;loop<ptcp->idat.fill;loop++) {
			if (loop%16==0) fprintf(data->plog,"\n%04x :",loop/16);
			fprintf(data->plog," %02x",ptcp->idat.data[loop]);
		}
		fprintf(data->plog,"\n");
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int tcp_request(void* that,void* conn,void* temp) {
	int loop, size;
	my1tcpsrv_t* ptcp = (my1tcpsrv_t*) that;
	my1conn_t* pcon = (my1conn_t*) conn;
	my1tcpdat_t* pdat = (my1tcpdat_t*) temp;
	/* show data */
	size = pdat->size-6;
	printf("-- UUID:{0x%04x}\n",pdat->uuid);
	printf("-- Type:{0x%04x}\n",pdat->type);
	printf("-- Size:{%d}\n##",size);
	for (loop=0;loop<size;loop++) 	{
		if (loop%16==0) printf("\n%04x :",loop/16);
		printf(" %02x",pdat->data.data[loop]);
	}
	printf("\n##\n");
	/* default reply - echo received data! */
	ptcp->tcpd.uuid = ~pdat->uuid;
	ptcp->tcpd.type = TCPDAT_TYPE_OK;
	if (pdat->type&TCPDAT_TYPE_TX)
		ptcp->tcpd.type |= TCPDAT_TYPE_RX;
	tcpdat_dopack(&ptcp->tcpd,&ptcp->odat);
	printf("-- REPLY:{%d}\n##",ptcp->odat.fill);
	for (loop=0;loop<ptcp->odat.fill;loop++) 	{
		if (loop%16==0) printf("\n%04x :",loop/16);
		printf(" %02x",ptcp->odat.data[loop]);
	}
	printf("\n## (%d)\n",ptcp->odat.fill);
	sock_send(pcon->sock,ptcp->odat.fill,(void*)ptcp->odat.data);
	return 0;
}
/*----------------------------------------------------------------------------*/
int tcp_found_no_data(void* that,void* conn,void* temp) {
	(void)that; (void)temp;
	my1conn_t* pcon = (my1conn_t*) conn;
	printf("Cannot get data from {%s}!\n",pcon->addr);
	return 0;
}
/*----------------------------------------------------------------------------*/
int tcp_found_errno(void* that,void* conn,void* temp) {
	(void)that;
	my1conn_t* pcon = (my1conn_t*) conn;
	int *pval = (int*)temp;
	printf("Cannot get socket data from {%s}! (%d)\n",pcon->addr,*pval);
	return 0;
}
/*----------------------------------------------------------------------------*/
int tcp_exec(void* that,void* conn,void* temp) {
	(void)conn; (void)temp;
	my1tcpsrv_t* ptcp = (my1tcpsrv_t*) that;
	printf("Running TCP server @port %d!\n",ptcp->server.port);
	printf("Press <ESC> to exit.\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
int tcp_done(void* that,void* conn,void* temp) {
	(void)conn; (void)temp;
	my1tcpsrv_t* ptcp = (my1tcpsrv_t*) that;
	printf("Server code done with code %d!\n",ptcp->server.flag);
	return 0;
}
/*----------------------------------------------------------------------------*/
int tcp_none(void* that,void* conn,void* temp) {
	(void)conn; (void)temp;
	my1tcpsrv_t* ptcp = (my1tcpsrv_t*) that;
	printf("Cannot run TCP server! (%d)\n",ptcp->server.sock.flag);
	return 0;
}
/*----------------------------------------------------------------------------*/
void data_init(data_t* data, int argc, char* argv[]) {
	int loop;
	/* initial/default settings */
	data->plog = 0x0;
	data->flag = 0;
	data->port = TCPSRV_PORT;
	for (loop=1;loop<argc;loop++) {
		if (argv[loop][0]=='-'&&argv[loop][1]=='-') {
			if (!strcmp(&argv[loop][2],"port")) {
				if (++loop<argc)
					data->port = atoi(argv[loop]);
				else {
					printf("No value for --port given!\n");
					data->flag |= ERROR_ARGS_PORT;
				}
			}
			else if (!strcmp(&argv[loop][2],"logs"))
				data->plog = stderr;
			else {
				printf("Unknown option '%s'!\n",argv[loop]);
				data->flag |= ERROR_ARGS_OPT;
			}
		}
		else {
			printf("Unknown parameter '%s'!\n",argv[loop]);
			data->flag |= ERROR_ARGS_PAR;
		}
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	data_t data;
	my1tcpsrv_t tcpsrv;
	data_init(&data,argc,argv);
	if (data.flag<0) return data.flag;
	data.plog = stderr;
	/* configure server */
	tcpsrv_init(&tcpsrv,data.port);
	tcpsrv.on_exec = tcp_exec;
	tcpsrv.on_done = tcp_done;
	tcpsrv.on_none = tcp_none;
	tcpsrv.on_connect = tcp_connect;
	tcpsrv.on_abort = tcp_abort;
	tcpsrv.on_request = tcp_request;
	tcpsrv.on_error = tcp_protocol_error;
	tcpsrv.on_zero_data = tcp_found_no_data;
	tcpsrv.on_chk_errno = tcp_found_errno;
	tcpsrv.data = (void*) &data;
	/* exec */
	tcpsrv_exec(&tcpsrv);
	/* done */
	tcpsrv_free(&tcpsrv);
	return 0;
}
/*----------------------------------------------------------------------------*/
