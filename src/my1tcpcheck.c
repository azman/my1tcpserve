/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "my1sockcli.h"
#include "my1tcpdat.h"
/*----------------------------------------------------------------------------*/
#define FLAG_HEXINPUT 0x01
#define READ_BUFFSIZE 4096
/*----------------------------------------------------------------------------*/
#define DEFAULT_PORT 1337
#define DEFAULT_HOST "localhost"
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop, test;
	unsigned int temp, flag;
	my1sockcli_t send;
	my1tcpdat_t tcpp;
	my1bytes_t pack, read;
	int port = DEFAULT_PORT;
	char host_def[] = DEFAULT_HOST;
	char *host = host_def;
	/* setup */
	sockcli_init(&send);
	tcpdat_init(&tcpp);
	bytes_init(&pack);
	bytes_init(&read);
	tcpp.type = TCPDAT_TYPE_TX; flag = 0;
	for (loop=1,test=0;loop<argc;loop++) {
		if (!strncmp(argv[loop],"--uuid",7)) {
			if (++loop<argc)
			{
				sscanf(argv[loop],"%x",&temp);
				tcpp.uuid = (unsigned short)temp;
			}
		}
		else if (!strncmp(argv[loop],"--type",7)) {
			if (++loop<argc) {
				sscanf(argv[loop],"%x",&temp);
				tcpp.type = (unsigned short)temp;
				tcpp.type |= TCPDAT_TYPE_TX; /* always set this tx bit */
			}
		}
		else if (!strncmp(argv[loop],"--host",7)) {
			loop++;
			host = argv[loop];
		}
		else if (!strncmp(argv[loop],"--port",7)) {
			loop++;
			port = atoi(argv[loop]);
		}
		else if (!strncmp(argv[loop],"--dhex",7)) {
			flag |= FLAG_HEXINPUT;
			test = loop + 1;
			break;
		}
		else if (!strncmp(argv[loop],"--data",7)) {
			test = loop + 1;
			break;
		}
	}
	if (test) {
		/* show these anyways */
		printf("-- UUID: 0x%04x!\n",tcpp.uuid);
		printf("-- Type: 0x%04x!\n",tcpp.type);
		printf("\nData: ");
		loop = test;
		while (loop<argc) {
			if (argv[loop][0]=='0'&&(argv[loop][1]=='x'||argv[loop][1]=='X')) {
				if(sscanf(argv[loop],"%x",&test)!=1)
					printf("\n** Error reading value '%s'!\n",argv[loop]);
			}
			else if (flag&FLAG_HEXINPUT) {
				if(sscanf(argv[loop],"%x",&test)!=1)
					printf("\n** Error reading value '%s'!\n",argv[loop]);
			}
			else {
				if(sscanf(argv[loop],"%d",&test)!=1)
					printf("\n** Error reading value '%s'!\n",argv[loop]);
			}
			if (test<0||test>0xff)
				printf("\n** Invalid byte '%s'!\n",argv[loop]);
			printf("%d (0x%02x) ",test,test);
			bytes_slip(&tcpp.data,(unsigned char)test&0xff);
			loop++;
		}
		bytes_make(&read,READ_BUFFSIZE); /** 4k seems ok */
		while (tcpdat_dopack(&tcpp,&pack)) { /* dummy loop actually! */
			printf("\nPack:");
			for (loop=0;loop<pack.fill;loop++)
				printf(" %02x",pack.data[loop]);
			printf("\n\n");
			/* setup host */
			if (!sockcli_host(&send,host,port)) {
				printf("** Invalid host '%s'!\n",host);
				break;
			}
			printf("@@ Host: %s@%s(%d)[%x]\n",send.host.addr,
				send.host.name,send.host.port,send.host.flag);
			if (sockcli_open(&send)) {
				printf("** Error connecting to host! (%x)\n",send.stat);
				break;
			}
			printf("-- Sending data... ");
			sockcli_send(&send,(void*)pack.data,pack.fill);
			printf("done.\n");
			printf("-- Reading... press <ESC> to exit.\n");
			test = sockcli_read(&send,(void*)read.data,read.size);
			if (test<=0) {
				printf("\n** Socket error? (%x)\n",send.stat);
				break;
			}
			read.fill = test;
			if (!tcpdat_unpack(&tcpp,&read)) {
				printf("** Invalid return packet (%d/%d)!\n##",
					read.fill,read.size);
				for (loop=0;loop<read.fill;loop++) {
					if (loop%16==0) printf("\n%04x :",loop/16);
					printf(" %02x",read.data[loop]);
				}
				printf("\n##\n");
				break;
			}
			/* show data */
			test = tcpp.size-6;
			printf("-- UUID:{0x%04x}\n",tcpp.uuid);
			printf("-- Type:{0x%04x}\n",tcpp.type);
			if (!(tcpp.type&TCPDAT_TYPE_RX))
				printf("   ** [WARN] No RX bit detected!\n");
			printf("-- Size:{%d}\n##",test);
			for (loop=0;loop<test;loop++) {
				if (loop%16==0) printf("\n%04x :",loop/16);
				printf(" %02x",tcpp.data.data[loop]);
			}
			printf("\n##\n");
			printf("@@\nPack:");
			for (loop=0;loop<read.fill;loop++)
				printf(" %02x",read.data[loop]);
			printf("\n@@\n");
			break; /* dummy loop */
		}
	}
	else printf("\n## No data to process!\n");
	bytes_free(&read);
	bytes_free(&pack);
	tcpdat_free(&tcpp);
	sockcli_free(&send);
	return 0;
}
/*----------------------------------------------------------------------------*/
